/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lenguaje;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 *
 * @author juan
 */
public class Lenguaje {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Uso: java -jar lenguaje <número de conjuntos de letras> <nombre del fichero>");
            return;
        }

        int numConjuntos = Integer.parseInt(args[0]);
        String nombreFichero = args[1];

        try (FileWriter writer = new FileWriter(nombreFichero)) {
            Random random = new Random();
            for (int i = 0; i < numConjuntos; i++) {
                int longitudConjunto = random.nextInt(20) + 2; // Longitud del conjunto entre 1 y 20
                for (int j = 0; j < longitudConjunto; j++) {
                    char letra = (char) ('a' + random.nextInt(26)); // Genera una letra aleatoria
                    writer.write(letra);
                }
                writer.write("\n");
            }
        } catch (IOException e) {
            System.out.println("Ha ocurrido un error al escribir en el fichero: " + e.getMessage());
        }
    }
}