/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package colabora;

/**
 *
 * @author juan
 */
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import lenguaje.Lenguaje;

public class Colaborar {
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Uso: java -jar colaborar <nombre del fichero>");
            return;
        }

        String nombreFichero = args[0];
        ExecutorService executor = Executors.newFixedThreadPool(10); // Crea un pool de hilos con 10 hilos

        for (int i = 1; i <= 10; i++) {
            int numConjuntos = i * 10;
            Runnable tarea = () -> {
                String[] argsLenguaje = {String.valueOf(numConjuntos), nombreFichero};
                lock.writeLock().lock(); // Bloquea para escritura
                try {
                    Lenguaje.main(argsLenguaje);
                } finally {
                    lock.writeLock().unlock(); // Desbloquea después de escribir
                }
            };
            executor.execute(tarea); // Ejecuta la tarea en un hilo del pool
        }

        executor.shutdown(); // Cierra el pool de hilos una vez que todas las tareas han terminado
    }
}

