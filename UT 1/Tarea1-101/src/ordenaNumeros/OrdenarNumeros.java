/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ordenaNumeros;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author juan
 */
public class OrdenarNumeros {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> num = new ArrayList<Integer>();

        System.out.println("Introduce números en una sola línea separados por un espacio. Cuando hayas terminado, pulsa enter:");

        String[] entrada = scanner.nextLine().split(" ");
        for (String str : entrada) {
            try {
                int numero = Integer.parseInt(str);
                num.add(numero);
            } catch (NumberFormatException e) {
                System.out.println("Entrada no válida: '" + str + "' no es un número.");
            }
        }

        Collections.sort(num);

        System.out.println("Números ordenados:");
        for (int numero : num) {
            System.out.println(numero);
        }
    }
}
