/**
 *
 * @author juan
 */
package Servidor;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {

    public static void main(String[] args) throws IOException {
        final int PUERTO = 1500;
        Socket socket = null;
        DataInputStream in;
        DataOutputStream out;
        File f = new File("listaTareas.txt");
        int idCliente = 0;

        try {
            ServerSocket servidor = new ServerSocket(PUERTO);
            System.out.println("Servidor iniciado");

            while (true) {
                // Pendiente de las conexiones del cliente
                socket = servidor.accept();

                //Comunicación de entrada y salida de datos con el cliente
                in = new DataInputStream(socket.getInputStream());
                out = new DataOutputStream(socket.getOutputStream());

                //Espera de mensaje que contiene le ID de cliente
                System.out.println(idCliente);
                idCliente = in.readInt();
                System.out.println("Conectado el cliente con id: " + idCliente);
                //Espera de instrucción del cliente
                String instruccion = in.readUTF();
                System.out.println(instruccion);

                if (instruccion.toLowerCase().equals("lista")) {
                    if (f.exists()) { //Comprobar que exista el fichero
                        if (f.canRead()) { //Comprobar que se pueda leer el fichero
                            BufferedReader br = new BufferedReader(new FileReader("listaTareas.txt"));

                            String linea = "";
                            String contenido = "";

                            while ((linea = br.readLine()) != null) {
                                contenido += linea + "\r\n";
                            }

                            br.close();

                            byte[] contenidoFichero = contenido.getBytes();

                            out.writeInt(contenidoFichero.length); // Enviando la longitud del fichero.

                            for (int i = 0; i < contenidoFichero.length; i++) {
                                out.writeByte(contenidoFichero[i]); //Se recorre por bloques de bytes y se envían a clientw
                            }

                        }

                    }

                } else if (instruccion.equals("cambioEstadoTarea")) {
                    String tarea = in.readUTF();
                    Estado estado = new Estado();
                    estado.cambioEstado(tarea);
                    out.writeUTF("Cambiado estado a completado");

                } else if (instruccion.equals("cierre")) {
                    socket.close();
                    System.out.println("Cliente desconectado");
                }

            }

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //finalización de la conexión socket con el cliente
            socket.close();
            System.out.println("Cliente desconectado");
        }

    }
}
