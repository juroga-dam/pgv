/**
 *
 * @author juan
 */
package Servidor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Estado {
    
    public void cambioEstado(String tarea) {
        // Fichero original y el fichero donde renombrará los nuevos cambios.
        File ficheroEntrada = new File("listaTareas.txt");
        File ficheroSalida = new File("tmp.txt");
        // Lectura y escritura de datos
        BufferedReader lector;
        BufferedWriter escritor;
        // String que almacenará cada línea para operar sobre ella. 
        String linea;
        try {
            // inicializar los Buffered con los ficheros de escritura y lectura.
            escritor = new BufferedWriter(new FileWriter(ficheroSalida));
            lector = new BufferedReader(new FileReader(ficheroEntrada));
            
            while ((linea = lector.readLine()) != null) {
                if (linea.contains(tarea)) {
                    // Sustitución de la línea que contiene la tarea indicada. 
                    String lineaNueva = linea.replaceFirst("pendiente", "COMPLETADO");
                    // añadimos en el fichero nuevo, la línea modificada.
                    escritor.append(lineaNueva + " \n");

                } else {
                    // Resto de tareas que no van a ser modificadas 
                    escritor.append(linea + " \n");
                }
            }
            
            // cierre de los buffered.
            escritor.close();
            lector.close();

        } catch (FileNotFoundException fnfe) {
            System.out.println("Fichero no encontrado");
        } catch (IOException ioe) {
            // ioe.printStackTrace();
        }
        // borrado del fichero original.
        ficheroEntrada.delete();
        // sustitución del fichero nuevo por el nombre del original. 
        ficheroSalida.renameTo(ficheroEntrada);
    }
}