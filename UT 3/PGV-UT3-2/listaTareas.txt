Instalar un sistema operativo en el servidor, COMPLETADA        
Conectar el servidor a la red, COMPLETADA        
Instalar y configurar el software del servidor de archivos, COMPLETADA        
Configurar los permisos de acceso, COMPLETADA        
Crear y configurar las carpetas compartidas, COMPLETADA        
Configurar la seguridad del servidor, COMPLETADA        
Configurar el servidor para que se inicie automaticamente al arrancar el sistema, pendiente        
Configurar el servidor para que se ejecute como un servicio, pendiente        
Configurar el servidor para que se reinicie automaticamente en caso de fallo, pendiente        
Configurar el servidor para que realice copias de seguridad automaticas, pendiente        
Configurar el servidor para que envie alertas por correo electronico en caso de fallo, pendiente        
Configurar el servidor para que permita el acceso remoto, pendiente        
Configurar el servidor para que permita el acceso a traves de Internet, pendiente        
Configurar el servidor para que permita el acceso a traves de VPN, pendiente        
Configurar el servidor para que permita el acceso a traves de FTP, COMPLETADO        
Configurar el servidor para que permita el acceso a traves de SSH, pendiente        
Configurar el servidor para que permita el acceso a traves de SMB, pendiente        
Configurar el servidor para que permita el acceso a traves de NFS, pendiente        
Configurar el servidor para que permita el acceso a traves de WebDAV, pendiente        
Configurar el servidor para que permita el acceso a traves de Rsync, pendiente        
