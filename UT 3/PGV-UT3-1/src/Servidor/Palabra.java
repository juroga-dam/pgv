/**
 *
 * @author juan
 */
package Servidor;

import java.util.concurrent.ThreadLocalRandom;

public class Palabra {

    public String cadenaAleatoria(int longitud) {
        // Muestra de caracteres
        String muestra = "abcdefghijklmnopqrstuvwxyz";

        // La cadena en donde iremos agregando un carácter aleatorio
        String cadena = "";
        for (int x = 0; x < longitud; x++) {
            int indiceAleatorio = (ThreadLocalRandom.current().nextInt(0, muestra.length() - 1) + 1);
            char caracterAleatorio = muestra.charAt(indiceAleatorio);
            cadena += caracterAleatorio;
        }
        return cadena;
    }

    public boolean comprobarCadena(String cadena, int longitud) {
        boolean resultado = false;
        if ((longitudCadena(cadena, longitud)) && (cadenaNoVacia(cadena))) {
            resultado = true;
        }
        return resultado;
    }

    public boolean longitudCadena(String cadena, int longitud) {
        boolean resultado = true;
        if ((cadena.length() > longitud) || (cadena.length() < longitud))
            resultado = false;
        return resultado;
        
    }

    public boolean cadenaNoVacia(String cadena) {
        return !cadena.isEmpty();
    }

    public String comparaCadenas(String cadenaAleatoria, String cadenaCliente) {
        String cadenaResult = "";
        String[] provisional = {"", "", "", ""};

        //Bucle que recorre la cadena intriducida por el cliente
        for (int i = 0; i < cadenaCliente.length(); i++) {
            //Se obtinene un caracter indicando la posición en base al valor de i del bucle
            String charCliente = (String) cadenaCliente.subSequence(i, i + 1);

            //Bucle que recorre la cadena generada aleatoramente
            for (int j = 0; j < cadenaAleatoria.length(); j++) {
                //Se obtinene un caracter indicando la posición en base al valor de j del bucle
                String charAleatoria = (String) cadenaAleatoria.subSequence(j, j + 1);

                //Compara los dos caracteres obtenidos si son iguales comprueba la posición
                if (charCliente.toLowerCase().compareTo(charAleatoria.toLowerCase()) == 0) {
                    if (i == j) { // Si están en la misma posición agrega el caracter de la cadena del cliente en mayúsculas
                        provisional[i] = (String) charCliente.toUpperCase();
                    } else { //Si ambos caracteres no están en la misma posición inserta el caracter entre paréntesis en el vector
                        provisional[i] = "(" + (String) charCliente.toLowerCase() + ")";
                    }
                } else { //En caso de no coincidir los caracteres se intruduce un asterisco en la posición del vector
                    if (provisional[i].equals("")) {
                        provisional[i] = "*";
                    }
                }
            }
        }
        // Se recorre el vector y se concatenan las Strings en una misma variable que se devuelve.
        for (int k = 0; k < provisional.length; k++) {
            cadenaResult = cadenaResult + provisional[k];
        }

        return cadenaResult;
    }
}
