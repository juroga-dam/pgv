/**
 *
 * @author juan
 */
package Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {

    public static void main(String[] args) {
        final int PUERTO = 2000;
        Socket socket = null;
        DataInputStream in;
        DataOutputStream out;
        int contIntento = 1, longitudCadena = 4;
        Palabra palabra = new Palabra();
        boolean result = false;
        String cadenaCliente;

        try {
            ServerSocket servidor = new ServerSocket(PUERTO);
            System.out.println("Servidor iniciado");

            while (true) {
                // Pendiente de las conexiones del cliente
                socket = servidor.accept();

                //Comunicación de entrada y salida de datos con el cliente
                in = new DataInputStream(socket.getInputStream());
                out = new DataOutputStream(socket.getOutputStream());

                //Espera de mensaje que contiene le ID de cliente
                int idCliente = in.readInt();
                System.out.println("Conectado el cliente con id: " + idCliente);

                //Recepción del número de intentos
                int intentos = in.readInt();
                System.out.println("El numero de intentos es: " + intentos);

                String palabraAleatoria = palabra.cadenaAleatoria(longitudCadena);
                System.out.println("La palabra a adivinar es: " + palabraAleatoria);
                out.writeUTF(palabraAleatoria); // Envia palabra a adivinar al clienta

                do {
                    //Recepción de la palabra
                    cadenaCliente = in.readUTF();
                    String resultComparacion = palabra.comparaCadenas(palabraAleatoria, cadenaCliente);

                    if (resultComparacion.toLowerCase().compareTo(cadenaCliente.toLowerCase()) == 0) {
                        out.writeUTF(resultComparacion); //Enviamos el resultado de la comparación
                        result = true;
                        out.writeBoolean(true);
                    } else {
                        out.writeUTF(resultComparacion); //Enviamos el resultado de la comparación
                        out.writeBoolean(false);
                    }

                    contIntento++;
                } while ((contIntento < intentos + 1) && (result == false)); //añadir si coincide la palabra cliente y aleatoria
                //} while ((contIntento < intentos) && (result == false));

                //finalización de la conexión socket con el cliente
                socket.close();
                System.out.println("Cliente desconectado");
            }

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
