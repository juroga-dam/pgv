/**
 *
 * @author juan
 */
package pgv.ut6.biblioteca;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

interface Estado {

    void ejecutar();
}

class EstadoOpcion1 implements Estado {

    @Override
    public void ejecutar() {
        System.out.println("Registro de usuarios");
        // Lógica específica para la Opción 1
    }
}

class EstadoOpcion2 implements Estado {

    @Override
    public void ejecutar() {
        System.out.println("Busqueda de libros");
        Scanner scanner = new Scanner(System.in);
        CargaDatos cargaDatos = new CargaDatos();

        System.out.println("Introduzca título o autor del libro a buscar");
        String buscado = scanner.next();

        String tituloAutor = "";
        Libro[] libros = cargaDatos.cargaLibros();

        for (int i = 0; i < libros.length; i++) {
            tituloAutor.concat(libros[i].getTitulo() + " " + libros[i].getAutor());
            if (tituloAutor.contains(buscado)) {
                System.out.println(String.format("Título: %s - Autor: %s", libros[i].getTitulo(), libros[i].getAutor()));
            } else {
                System.out.println("No se encuentra libros con el criterio de búsqueda indicado.");
            }          
        }
        // Lógica específica para la Opción 2
    }
}

class EstadoOpcion3 implements Estado {

    @Override
    public void ejecutar() {
        System.out.println("Prestamo de libros");
        // Lógica específica para la Opción 2
    }
}

class EstadoOpcion4 implements Estado {

    @Override
    public void ejecutar() {
        System.out.println("Devolucion de libros");
        // Lógica específica para la Opción 2
    }
}

class EstadoSalir implements Estado {

    @Override
    public void ejecutar() {
        System.out.println("Saliendo del programa");
        System.exit(0);
    }
}

public class Biblioteca {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Estado> estados = new HashMap<>();
        estados.put("1", new EstadoOpcion1());
        estados.put("2", new EstadoOpcion2());
        estados.put("3", new EstadoOpcion3());
        estados.put("4", new EstadoOpcion4());
        estados.put("5", new EstadoSalir());

        String estadoActual = null;

        /*CargaDatos cargaDatos = new CargaDatos();               
        Usuario[] usuarios = cargaDatos.cargaUsuarios();
        Libro[] libros = cargaDatos.cargaLibros();*/
        while (true) {
            System.out.println("Menu:");
            System.out.println("1. Registro de usuarios");
            System.out.println("2. Busqueda de libros");
            System.out.println("3. Prestamo de libros");
            System.out.println("4. Devolucion de libros");
            System.out.println("5. Salir");

            System.out.print("Seleccione una opcion mediante su numero: ");
            String seleccion = scanner.next();

            if (estados.containsKey(seleccion)) {
                estadoActual = seleccion;
                estados.get(estadoActual).ejecutar();
            } else {
                System.out.println("Opcion no valida. Intentelo de nuevo.");
            }

            if ("5".equals(estadoActual)) {
                break;
            }
        }
    }
}
