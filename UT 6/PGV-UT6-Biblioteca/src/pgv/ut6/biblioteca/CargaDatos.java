/**
 *
 * @author juan
 */
package pgv.ut6.biblioteca;

public class CargaDatos {

    public Usuario[] cargaUsuarios() {

        //Se generan los usuarios
        Usuario usuario1 = new Usuario("Juan", "Pérez García", "12345678A");
        Usuario usuario2 = new Usuario("Ana", "López Martínez", "23456789B");
        Usuario usuario3 = new Usuario("Pedro", "Gómez Herrera", "34567890C");
        Usuario usuario4 = new Usuario("María", "Torres Ruiz", "45678901D");
        Usuario usuario5 = new Usuario("Carlos", "Sánchez Jiménez", "56789012E");

        Usuario[] usuarios = {usuario1, usuario2, usuario3, usuario4, usuario5};

        return usuarios;
    }

    public Libro[] cargaLibros() {

        //Se generan los libros
        Libro libro1 = new Libro(1, "Génesis", "Eduardo Chillida", "978-84-89727-00-72", 1973, 3, null);
        Libro libro2 = new Libro(2, "El jardín de las delicias", "Francisco Umbral", "978-84-233-0000-0", 1975, 2, null);
        Libro libro3 = new Libro(3, "La ciudad y los perros", "Mario Vargas Llosa", "978-84-204-7007-8", 1963, 4, null);
        Libro libro4 = new Libro(4, "Cien años de soledad", "Gabriel García Márquez", "978-84-204-7007-8", 1967, 2, null);
        Libro libro5 = new Libro(5, "El otoño del patriarca", "Gabriel García Márquez", "978-84-204-7007-8", 1975, 1, null);
        Libro libro6 = new Libro(6, "La casa de los espíritus", "Isabel Allende", "978-84-204-7007-8", 1982, 3, null);
        Libro libro7 = new Libro(7, "El amor en los tiempos del cólera", "Gabriel García Márquez", "978-84-204-7007-8", 1985, 2, null);
        Libro libro8 = new Libro(8, "La sombra del viento", "Carlos Ruiz Zafón", "978-84-204-7007-8", 2001, 3, null);
        Libro libro9 = new Libro(9, "La catedral del mar", "Ildefonso Falcones", "978-84-204-7007-8", 2006, 4, null);
        Libro libro10 = new Libro(10, "La reina del sur", "Arturo Pérez-Reverte", "978-84-204-7007-8", 2002, 2, null);

        Libro[] libros = {libro1, libro2, libro3, libro4, libro5, libro6, libro7, libro8, libro9, libro10};

        return libros;
    }
}
