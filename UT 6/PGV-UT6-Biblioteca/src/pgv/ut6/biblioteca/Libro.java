/**
 *
 * @author juan
 */
package pgv.ut6.biblioteca;


public class Libro {
    
    private int idLibro;
    private String titulo;
    private String autor;
    private String ISBN;
    private int disponibles;
    private int anioPublicacion;
    private Usuario usuario;

    public Libro() {
    }

    public Libro(int idLibro, String titulo, String autor, String ISBN, int disponibles, int anioPublicacion, Usuario usuario) {
        this.idLibro = idLibro;
        this.titulo = titulo;
        this.autor = autor;
        this.ISBN = ISBN;
        this.disponibles = disponibles;
        this.anioPublicacion = anioPublicacion;
        this.usuario = usuario;
    }

    public int getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(int idLibro) {
        this.idLibro = idLibro;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public int getDisponibles() {
        return disponibles;
    }

    public void setDisponibles(int disponibles) {
        this.disponibles = disponibles;
    }

    public int getAnioPublicacion() {
        return anioPublicacion;
    }

    public void setAnioPublicacion(int anioPublicacion) {
        this.anioPublicacion = anioPublicacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    
    

}
