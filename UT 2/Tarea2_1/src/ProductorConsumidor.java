
import static java.lang.Thread.sleep;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author juan
 */
public class ProductorConsumidor {
    public static void main(String[] args) {
        
        try {
            Almacen b = new Almacen(6);
            Productor p = new Productor(b);
            Consumidor c = new Consumidor(b);
            
            p.start();
        
            Thread.sleep(3000);
        
            c.start();
            
            p.join();
            c.join();
            System.out.println("Fin programa");
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(ProductorConsumidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }    
        
        
    }
}

class Almacen {
 
    private final char[] buffer;
    private int siguiente;
    private boolean lleno;
    private boolean vacio;
    
    public Almacen(int tamanio){
        this.buffer = new char[tamanio];
        this.siguiente = 0;
        this.lleno = false;
        this.vacio = true;
    }
    
    public synchronized char consumir(){
        
        while(this.vacio){
            try {
                wait();
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(Almacen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
            
        
        this.siguiente--;
        this.lleno = false;
        
        if(this.siguiente == 0){
            this.vacio = true;
        }
        
        notifyAll();
        
        return this.buffer[this.siguiente];
        
    }
    
    public synchronized void producir(char c){
        
        while(this.lleno){
            try {
                wait();
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(Almacen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        
        this.buffer[this.siguiente] = c;
        
        this.siguiente++;
        this.vacio = false;
        
        if(this.siguiente == this.buffer.length){
            this.lleno = true;
        }
        
        notifyAll();
        
    }
    
}

class Productor extends Thread {

    private final Almacen buffer;
    private final String letras = "abcdefghijklmnopqrstuvxyz";
    private int produced;
    private final int LIMIT = 15;
    
    public Productor(Almacen buffer){
        this.produced = 0;
        this.buffer = buffer;
    }
    
    @Override
    public void run(){
        while(produced < LIMIT){
            
            try {
                char c = letras.charAt((int)(Math.random() * letras.length()));
                buffer.producir(c);
                produced++;
                System.out.println("Producido el caracter " + c);
                sleep((long) (Math.random() * 4000));
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(Productor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            
            
        }
    }

}

class Consumidor extends Thread {
    
    private final Almacen buffer;
    private int consumed;
    private final int LIMIT = 15;
    
    public Consumidor(Almacen buffer){
        this.consumed = 0;
        this.buffer = buffer;
    }
    
    @Override
    public void run(){
        while(consumed < LIMIT){
            
                char c = buffer.consumir();
                consumed++;
                System.out.println("Consumido el caracter " + c + " del almacenamiento");
            try {
                sleep((long) (Math.random() * 4000));
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(Consumidor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            
        }
    }
}