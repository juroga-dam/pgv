/**
 *
 * @author juan
 */
package Cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import Servidor.Palabra;

public class Cliente {

    public static void instrucciones() {
        System.out.println("     JUEGO ADIVINA EL ORDEN DE LAS LETRAS         ");
        System.out.println("--------------------------------------------------");
        System.out.println("\n              -- INSTRUCIONES  --                 ");
        System.out.println(" Se comenzara indicando el numero de intentos que se\ndesea para intentar adivinar el orden de las palabras\n\nSe inicia con un primer intento y se respondera con\nun resultado que dara pistas sobre las letras a adivinar\n\nLas letras que no estan en la cadena final apareceran\ncon un asterisco (*)\n\nLas letras que si estan el la cadena a adivinar apareceran\nde dos maneras:\n\n- Si esta en la posicion correcta aparecera en mayusculas\n- Si existe la letra pero no esta en la posicion aparece\nentre parentesis");
    }

    public static int solicitarIntentos() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Introducza el numero de intentos que desea: ");
        int intentos = 0;
        return intentos = scan.nextInt();
    }

    public static void main(String[] args) {
        final String HOST = "127.0.0.1";
        final int PUERTO = 2000;
        DataInputStream in;
        DataOutputStream out;
        int min = 1000, max = 9999, intentos = 0, contIntentos = 1, numCaracteres = 4;
        int idCliente = ThreadLocalRandom.current().nextInt(min, max + 1);
        String palabraCliente = "";
        Scanner scan = new Scanner(System.in);
        boolean resultado = false, result = false;
        Palabra palabra = new Palabra();
        String carRes = "";

        try {
            //Creación del socket de comunicación con el servidor
            Socket socket = new Socket(HOST, PUERTO);

            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());

            System.out.println("El ID de cliente es:" + idCliente);

            //Envío de primer mensaje a servidor con el idCliente
            out.writeInt(idCliente);

            //Mostrar las instrucciones del juego
            instrucciones();

            //Solicitud de intentos
            intentos = solicitarIntentos();

            //Envío de intentos al servidor
            out.writeInt(intentos);

            //Recepción de la palabra aleatoria
            String cadenaAdivinar = in.readUTF();

            do { // Comprobar que la palabra introducida tiene 4 letras
                System.out.print("Introduzca una palabra de 4 letras: ");
                palabraCliente = scan.next();
                if (!(palabra.comprobarCadena(palabraCliente, numCaracteres))) {
                    result = false;
                    System.out.println("Cadena de letras incorrecta");
                } else {
                    result = true;
                }
            } while (result != true);

            do { //Se realizarán los intentos indicados por usuario

                if (contIntentos > 1) {
                    System.out.print("Intento numero " + contIntentos + ": ");
                    palabraCliente = scan.next();
                }

                //Envío de la palabra
                out.writeUTF(palabraCliente);

                //Recepción de los caracteres resultado de comparación
                carRes = in.readUTF();
                System.out.println(carRes);

                //Recepción del resultado, si se ha ganado se muestra mensaje por pantalla y finaliza el juego
                resultado = in.readBoolean();
                if (resultado) {
                    System.out.println("HAS GANADO");
                }

                contIntentos++;

                if ((contIntentos > intentos) && (resultado == false)) {
                    System.out.println("Se ha llegado al maximo de intentos y no se ha adivinado las letras");
                    System.out.println("La cadena a adivinar era: " + cadenaAdivinar);
                }
            } while ((contIntentos <= intentos) && (resultado == false));

            //cerrar conexión del socket
            out.writeInt(idCliente); // Se envía de nuevo el idCliente para identificarlo en el servidor
            socket.close();

        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
