/**
 *
 * @author juan
 */
package Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServidorHilo extends Thread {

    private DataInputStream in;
    private DataOutputStream out;
    private int idCliente;

    public ServidorHilo(DataInputStream in, DataOutputStream out, int idCliente) {
        this.in = in;
        this.out = out;
        this.idCliente = idCliente;
    }

    @Override
    public void run() {
        int contIntento = 1, longitudCadena = 4;
        Palabra palabra = new Palabra();
        boolean result = false;
        String cadenaCliente;

        try {
            //Recepción del número de intentos
            int intentos = in.readInt();
            //System.out.println("El numero de intentos es: " + intentos);

            String palabraAleatoria = palabra.cadenaAleatoria(longitudCadena);
            System.out.println(String.format("La palabra a adivinar para el cliente %s es: %s", idCliente, palabraAleatoria));
            out.writeUTF(palabraAleatoria); // Envia palabra a adivinar al clienta

            do {
                //Recepción de la palabra
                cadenaCliente = in.readUTF();
                String resultComparacion = palabra.comparaCadenas(palabraAleatoria, cadenaCliente);

                if (resultComparacion.toLowerCase().compareTo(cadenaCliente.toLowerCase()) == 0) {
                    out.writeUTF(resultComparacion); //Enviamos el resultado de la comparación
                    result = true;
                    out.writeBoolean(true);
                } else {
                    out.writeUTF(resultComparacion); //Enviamos el resultado de la comparación
                    out.writeBoolean(false);
                }

                contIntento++;
            } while ((contIntento < intentos + 1) && (result == false)); //añadir si coincide la palabra cliente y aleatoria
            //} while ((contIntento < intentos) && (result == false));

            idCliente = in.readInt();
            System.out.println(String.format("Cliente con id: %s se ha desconectado", idCliente));
        } catch (IOException ex) {
            Logger.getLogger(ServidorHilo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
