/**
 *
 * @author juan
 */
package Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {

    public static void main(String[] args) {
        final int PUERTO = 2000;
        Socket socket = null;
        DataInputStream in;
        DataOutputStream out;

        try {
            ServerSocket servidor = new ServerSocket(PUERTO);
            System.out.println("Servidor iniciado");

            while (true) {
                // Pendiente de las conexiones del cliente
                socket = servidor.accept();

                //Comunicación de entrada y salida de datos con el cliente
                in = new DataInputStream(socket.getInputStream());
                out = new DataOutputStream(socket.getOutputStream());

                //Espera de mensaje que contiene le ID de cliente
                int idCliente = in.readInt();
                System.out.println("Conectado el cliente con id: " + idCliente);

                ServidorHilo hilo = new ServidorHilo(in, out, idCliente);
                hilo.start();
            }

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
