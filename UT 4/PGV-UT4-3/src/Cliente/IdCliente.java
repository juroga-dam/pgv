/**
 *
 * @author juan
 */
package Cliente;

import java.util.concurrent.ThreadLocalRandom;

public class IdCliente {

    public int generarId(){
        int min = 1000, max = 9999;
        int idCliente = ThreadLocalRandom.current().nextInt(min, max + 1);
        return idCliente;
    }
}
