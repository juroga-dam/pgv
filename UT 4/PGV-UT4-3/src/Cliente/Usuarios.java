/**
 *
 * @author juan
 */
package Cliente;


public class Usuarios {
    
    private String user;
    private String password;

    public Usuarios(String user, String password) {
        this.user = user;
        this.password = password;
    }
    
    public Usuarios() {

    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
