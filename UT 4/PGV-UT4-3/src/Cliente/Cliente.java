/**
 *
 * @author juan
 */
package Cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cliente {

    public static void main(String[] args) {
        final String HOST = "127.0.0.1";
        final int PUERTO = 1500;
        DataInputStream in;
        DataOutputStream out;
        IdCliente idCliente = new IdCliente();
        String instruccion;
        Scanner scanner = new Scanner(System.in);
        Login login = new Login();
        int opcion;
        boolean resultLogin = false;

        //Creación del socket de comunicación con el servidor
        Socket socket;

        try {
            socket = new Socket(HOST, PUERTO);
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());

            int idClient = idCliente.generarId();
            System.out.println("El ID de cliente es:" + idClient);

            //Envío de primer mensaje a servidor con el idCliente
            out.writeInt(idClient);

            do {
                String user = login.login().getUser();
                String password = login.login().getPassword();
                
                System.out.println(String.format("EL USUARIO INTRODUCIDO ES: %s Y LA CONTRASEÑA %s", user, password));
                out.writeUTF(user);             
                out.writeUTF(password);
                
                resultLogin = in.readBoolean(); //Espera el resultado del login
                System.out.println("El resultado del login es: "+resultLogin);
                if (resultLogin == false){
                    System.out.println("Error en el usuario o contrasenia");
                }
            } while (!resultLogin);

            while (true) {
                System.out.println();
                System.out.println("Menu:");
                System.out.println("1. Mostrar lista de tareas");
                System.out.println("2. Cambiar estado de una tarea");
                System.out.println("3. Salir");

                System.out.print("Seleccione una opcion mediante su numero: ");
                opcion = scanner.nextInt();

                switch (opcion) {
                    case 1:
                        System.out.print("Para visualizar la lista de tareas escriba 'lista': ");
                        instruccion = scanner.next();

                        out.writeUTF(instruccion);
                        int longitud = in.readInt(); //Se recibe la longitud del fichero para poderlo recorrer

                        byte[] contenido = new byte[longitud];

                        for (int i = 0; i < longitud; i++) {
                            contenido[i] = in.readByte(); //Se reciben los bloques de bytes
                        }

                        String contenidoFichero = new String(contenido); //Se pasan los bytes a tipo String

                        System.out.println(contenidoFichero);
                        break;
                    case 2:
                        instruccion = "cambioEstadoTarea";
                        out.writeUTF(instruccion);

                        System.out.println("Introduzca la tarea completa para cambiar el estado");
                        System.out.print("Tarea: ");
                        String tarea = scanner.next();
                        out.writeUTF(tarea);
                        System.out.println(in.readUTF());
                        break;
                    case 3:
                        System.out.println("Saliendo del programa");
                        instruccion = "cierre";
                        out.writeUTF(instruccion);
                        //cerrar conexión del socket
                        socket.close();
                        System.exit(0);
                    default:
                        System.out.println("Opcion no valida. Intentelo de nuevo.");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
