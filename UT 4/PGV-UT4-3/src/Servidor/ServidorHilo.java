/**
 *
 * @author juan
 */
package Servidor;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServidorHilo extends Thread {

    private DataInputStream in;
    private DataOutputStream out;
    private int idCliente;

    public ServidorHilo(DataInputStream in, DataOutputStream out, int idCliente) {
        this.in = in;
        this.out = out;
        this.idCliente = idCliente;
    }

    @Override
    public void run() {
        try {
            File f = new File("listaTareas.txt");
            SSO sso = new SSO();
            boolean accessPermited = false;

            do {
                //Se reciben las credenciales
                String usuario = in.readUTF();
                String contrasena = in.readUTF();

                if (sso.login(usuario, contrasena)) { //Accede al condicional si el valor devuleto es 'true' en la comprobación de credenciale4s
                    accessPermited = true;
                    out.writeBoolean(true); // Respuesta positiva al cliente para el acceso con credenciales

                    System.out.println(String.format("El usuario %s de ha conectado correctamente", usuario));
                    //Espera de instrucción del cliente
                    String instruccion = in.readUTF();
                    System.out.println(instruccion);

                    if (instruccion.toLowerCase().equals("lista")) {
                        if (f.exists()) { //Comprobar que exista el fichero
                            if (f.canRead()) { //Comprobar que se pueda leer el fichero
                                BufferedReader br = new BufferedReader(new FileReader("listaTareas.txt"));

                                String linea = "";
                                String contenido = "";

                                while ((linea = br.readLine()) != null) {
                                    contenido += linea + "\r\n";
                                }

                                br.close();

                                byte[] contenidoFichero = contenido.getBytes();

                                out.writeInt(contenidoFichero.length); // Enviando la longitud del fichero.

                                for (int i = 0; i < contenidoFichero.length; i++) {
                                    out.writeByte(contenidoFichero[i]); //Se recorre por bloques de bytes y se envían a clientw
                                }

                            }

                        }

                    } else if (instruccion.equals("cambioEstadoTarea")) {
                        String tarea = in.readUTF();
                        Estado estado = new Estado();
                        estado.cambioEstado(tarea);
                        out.writeUTF("Cambiado estado a completado");

                    } else if (instruccion.equals("cierre")) {
                        System.out.println(String.format("Cliente con id: %s se ha desconectado", idCliente));
                    }
                } else {
                    accessPermited = false;
                    System.out.println(String.format("El cliente %s ha enviado una peticion de login del usuario %s con resultado erroneo", idCliente, usuario));
                    out.writeBoolean(false); // Se envía falso tras el resultado negativo de acceso.
                }
            } while (accessPermited); 
        } catch (IOException ex) {
            Logger.getLogger(ServidorHilo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
